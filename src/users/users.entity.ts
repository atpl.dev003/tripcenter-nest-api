import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne
} from 'typeorm';
import { Roles } from '../roles/roles.entity';
// import {ApplicationLogs} from "./ApplicationLogs";
// import {ClientUsers} from "./ClientUsers";
// import {DailyTasks} from "./DailyTasks";
// import {LetsAdmin} from "./LetsAdmin";
// import {Messages} from "./Messages";
// import {TourGuides} from "./TourGuides";

@Entity('Users', { schema: 'dbo' })
@Index('IX_Users->Email', ['Email'])
export class Users {
  @Column('uniqueidentifier', {
    nullable: false,
    primary: true,
    name: 'Id'
  })
  Id: string;

  @Column('nvarchar', {
    nullable: false,
    length: 200,
    name: 'Email'
  })
  Email: string;

  @Column('nvarchar', {
    nullable: false,
    length: 200,
    name: 'Password'
  })
  Password: string;

  @ManyToOne(() => Roles, (Roles: Roles) => Roles.users, { nullable: false })
  @JoinColumn({ name: 'Role' })
  role: Roles | null;

  @Column('datetime', {
    nullable: false,
    name: 'DateCreated'
  })
  DateCreated: Date;

  @Column('datetime', {
    nullable: true,
    name: 'DateAcceptedTermsAndConditions'
  })
  DateAcceptedTermsAndConditions: Date | null;

  @Column('bit', {
    nullable: true,
    name: 'IsSuspend'
  })
  IsSuspend: boolean | null;

  @Column('varchar', {
    nullable: true,
    length: 300,
    default: () => '(0)',
    name: 'MenuList'
  })
  MenuList: string | null;

  @Column('varchar', {
    nullable: true,
    length: 500,
    default: () => '(1)',
    name: 'Manager'
  })
  Manager: string | null;

  @Column('bit', {
    nullable: true,
    name: 'PasswordReset'
  })
  PasswordReset: boolean | null;

  @Column('bit', {
    nullable: true,
    name: 'IsBToE'
  })
  IsBToE: boolean | null;

  @Column('bit', {
    nullable: true,
    name: 'PasswordAutoReset'
  })
  PasswordAutoReset: boolean | null;

  @Column('varchar', {
    nullable: true,
    length: 500,
    name: 'JobTitle'
  })
  JobTitle: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'TelPhone'
  })
  TelPhone: string | null;

  @Column('varchar', {
    nullable: true,
    length: 500,
    name: 'Emergency'
  })
  Emergency: string | null;

  @Column('varchar', {
    nullable: true,
    length: 500,
    name: 'Department'
  })
  Department: string | null;

  @Column('varchar', {
    nullable: true,
    length: 10,
    name: 'Salutation'
  })
  Salutation: string | null;

  @Column('varchar', {
    nullable: true,
    length: 100,
    name: 'fName'
  })
  fName: string | null;

  @Column('varchar', {
    nullable: true,
    length: 100,
    name: 'lName'
  })
  lName: string | null;

  @Column('varchar', {
    nullable: true,
    length: 100,
    name: 'CountryCode'
  })
  CountryCode: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'Currency'
  })
  Currency: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'Distance'
  })
  Distance: string | null;

  // @OneToMany(()=>ApplicationLogs, (ApplicationLogs: ApplicationLogs)=>ApplicationLogs.user)
  // applicationLogss:ApplicationLogs[];

  // @OneToOne(()=>ClientUsers, (ClientUsers: ClientUsers)=>ClientUsers.,{ onDelete: 'CASCADE' , })
  // clientUsers:ClientUsers | null;

  // @OneToMany(()=>DailyTasks, (DailyTasks: DailyTasks)=>DailyTasks.createdBy)
  // dailyTaskss:DailyTasks[];

  // @OneToMany(()=>DailyTasks, (DailyTasks: DailyTasks)=>DailyTasks.completedBy)
  // dailyTaskss2:DailyTasks[];

  // @OneToOne(()=>LetsAdmin, (LetsAdmin: LetsAdmin)=>LetsAdmin.,{ onDelete: 'CASCADE' , })
  // letsAdmin:LetsAdmin | null;

  // @OneToMany(()=>Messages, (Messages: Messages)=>Messages.addedBy)
  // messagess:Messages[];

  // @OneToOne(()=>TourGuides, (TourGuides: TourGuides)=>TourGuides.,{ onDelete: 'CASCADE' , })
  // tourGuides:TourGuides | null;
}
