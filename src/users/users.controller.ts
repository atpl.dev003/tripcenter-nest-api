import { Get, Post, Delete, Param, Controller } from '@nestjs/common';

import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { UserService } from './users.service';
import { Users } from './users.entity';

@ApiBearerAuth()
@ApiUseTags('user')
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get()
  async getUsers(): Promise<Users[]> {
    return this.userService.findAll();
  }

  // @Get(':username')
  // async getProfile(@Users('id') userId: number, @Param('username') username: string): Promise<ProfileRO> {
  //     return await this.profileService.findProfile(userId, username);
  // }

  // @Post(':username/follow')
  // async follow(@User('email') email: string, @Param('username') username: string): Promise<ProfileRO> {
  //     return await this.profileService.follow(email, username);
  // }

  // @Delete(':username/follow')
  // async unFollow(@User('id') userId: number, @Param('username') username: string): Promise<ProfileRO> {
  //     return await this.profileService.unFollow(userId, username);
  // }
}
