import { Roles } from '../roles/roles.entity';

export interface UserRO {
  Id: string;
  Email: string;
  Role: Roles;
  Name: string;
  JobTitle: string;
  Telephone: string;
  Department: string;
  FName: string;
  LName: string;
  CountryCode: string;
  Currency: string;
  Token: string;
}
