import { HttpStatus, Injectable, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { DeepPartial } from 'typeorm/common/DeepPartial';
import { Users } from './users.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(Users)
    private readonly userRepository: Repository<Users> // @InjectRepository(FollowsEntity) // private readonly followsRepository: Repository<FollowsEntity>
  ) {}

  async findAll(): Promise<Users[]> {
    return await this.userRepository.find();
  }

  async findOne(options?: DeepPartial<Users>): Promise<Users> {
    const user = await this.userRepository.findOne(options, {
      relations: ['role']
    });
    return user;
  }

  async findById(id: number): Promise<Users> {
    const user = await this.userRepository.findOne(id);

    if (!user) {
      const errors = { User: ' not found' };
      throw new HttpException({ errors }, 401);
    }

    return user;
  }
}
