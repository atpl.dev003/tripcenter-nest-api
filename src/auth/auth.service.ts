import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { DeepPartial } from 'typeorm/common/DeepPartial';
import { Users } from '../users/users.entity';
import { LoginUserDto } from './dtos/login-request.dto';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(Users)
    private readonly _repository: Repository<Users>
  ) {}

  async getUser(options: DeepPartial<LoginUserDto>): Promise<Users> {
    const findOneOptions = {
      Email: options.email
    };
    const user = await this._repository.findOne(findOneOptions, {
      relations: ['role']
    });
    return user;
  }
}
