import { IsNotEmpty, IsEmail } from 'class-validator';

export class SupplierRegistraionDto {
  @IsNotEmpty()
  @IsEmail()
  readonly email: string;

  @IsNotEmpty()
  readonly confirmEmail: string;

  @IsNotEmpty()
  readonly password: string;
}
