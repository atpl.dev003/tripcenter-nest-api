import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import {
  Controller,
  Post,
  UsePipes,
  ValidationPipe,
  Body,
  HttpException,
  HttpStatus
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginUserDto } from './dtos/login-request.dto';
import { BaseController } from '../shared/base.controller';
import { IBaseResponse } from '../shared/interfaces/IBaseResponse';
import { UserRO } from '../users/users.interface';
import { encryptString } from '../shared/utils/string.utils';
import { generateJWT } from '../shared/utils/jwt.utils';

@ApiBearerAuth()
@ApiUseTags('auth')
@Controller('auth')
export class AuthController extends BaseController {
  constructor(private readonly _service: AuthService) {
    super();
  }

  @UsePipes(new ValidationPipe())
  @Post('supplier/login')
  async supplierLogin(
    @Body() loginUserDto: LoginUserDto
  ): Promise<IBaseResponse<UserRO>> {
    const _user = await this._service.getUser(loginUserDto);

    // if User not found
    if (!_user)
      throw new HttpException(
        this.buildResponse(
          HttpStatus.BAD_REQUEST,
          'We cannot find your account along with this email.'
        ),
        HttpStatus.BAD_REQUEST
      );

    // if password wrong
    if (_user.Password !== loginUserDto.password)
      //encryptString(loginUserDto.password)
      throw new HttpException(
        this.buildResponse(
          HttpStatus.BAD_REQUEST,
          'You entered wrong password.'
        ),
        HttpStatus.BAD_REQUEST
      );

    // if User Suspended
    if (_user.IsSuspend)
      throw new HttpException(
        this.buildResponse(
          HttpStatus.BAD_REQUEST,
          'Your account has temporary suspended. please contact tripcenter admin.'
        ),
        HttpStatus.BAD_REQUEST
      );

    // if not supplier access
    if (this.CONST_SUPPLIER_ROLES.includes(_user.role.Id))
      throw new HttpException(
        this.buildResponse(
          HttpStatus.BAD_REQUEST,
          `Restricted area. You don't have an access`
        ),
        HttpStatus.BAD_REQUEST
      );

    const _userRO = generateJWT(_user);

    return this.buildResponse(200, 'Login succesfull', _userRO);
  }

  @UsePipes(new ValidationPipe())
  @Post('cms/login')
  async cmsLogin(
    @Body() loginUserDto: LoginUserDto
  ): Promise<IBaseResponse<UserRO>> {
    const _user = await this._service.getUser(loginUserDto);

    // if User not found
    if (!_user)
      throw new HttpException(
        this.buildResponse(
          HttpStatus.BAD_REQUEST,
          'We cannot find your account along with this email.'
        ),
        HttpStatus.BAD_REQUEST
      );

    // if password wrong
    if (_user.Password !== loginUserDto.password)
      //encryptString(loginUserDto.password)
      throw new HttpException(
        this.buildResponse(
          HttpStatus.BAD_REQUEST,
          'You entered wrong password.'
        ),
        HttpStatus.BAD_REQUEST
      );

    // if User Suspended
    if (_user.IsSuspend)
      throw new HttpException(
        this.buildResponse(
          HttpStatus.BAD_REQUEST,
          'Your account has temporary suspended. please contact tripcenter admin.'
        ),
        HttpStatus.BAD_REQUEST
      );

    // if not cms access
    if (this.CONST_CMS_ROLES.includes(_user.role.Id))
      throw new HttpException(
        this.buildResponse(
          HttpStatus.BAD_REQUEST,
          `Restricted area. You don't have an access`
        ),
        HttpStatus.BAD_REQUEST
      );

    const _userRO = generateJWT(_user);

    return this.buildResponse(200, 'Login succesfull', _userRO);
  }
}
