import { Get, Controller } from '@nestjs/common';
import { BaseController } from './shared/base.controller';

@Controller('api')
export class AppController extends BaseController {
  @Get()
  root(): string {
    return 'Hello World!';
  }
}
