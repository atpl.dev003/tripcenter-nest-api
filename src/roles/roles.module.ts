import { Module } from '@nestjs/common';
import { RoleService } from './roles.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Roles } from './roles.entity';
import { RoleController } from './role.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Roles])],
  providers: [RoleService],
  controllers: [RoleController]
})
export class RoleModule {}
