import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { DeepPartial } from 'typeorm/common/DeepPartial';
import { Roles } from './roles.entity';

@Injectable()
export class RoleService {
  constructor(
    @InjectRepository(Roles)
    private readonly userRepository: Repository<Roles>
  ) // @InjectRepository(FollowsEntity)
  // private readonly followsRepository: Repository<FollowsEntity>
  {}

  async findAll(): Promise<Roles[]> {
    return await this.userRepository.find();
  }

  async findOne(options?: DeepPartial<Roles>): Promise<Roles> {
    const user = await this.userRepository.findOne(options);
    return user;
  }
}
