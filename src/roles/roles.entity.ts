import { Column, Entity, OneToMany } from 'typeorm';
import { Users } from '../users/users.entity';
// import { Users } from './users.entisty';

@Entity('Roles', { schema: 'dbo' })
export class Roles {
  @Column('uniqueidentifier', {
    nullable: false,
    primary: true,
    name: 'Id'
  })
  Id: string;

  @Column('nvarchar', {
    nullable: false,
    length: 200,
    name: 'Key'
  })
  Key: string;

  @Column('nvarchar', {
    nullable: false,
    length: 200,
    name: 'DisplayValue'
  })
  DisplayValue: string;

  @Column('bit', {
    nullable: false,
    name: 'IsSuperUser'
  })
  IsSuperUser: boolean;

  @OneToMany(() => Users, (Users: Users) => Users.role)
  users: Users[];
}
