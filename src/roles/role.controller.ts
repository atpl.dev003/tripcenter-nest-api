import { Get, Post, Delete, Param, Controller } from '@nestjs/common';

import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { RoleService } from './roles.service';
import { Roles } from './roles.entity';

@ApiBearerAuth()
@ApiUseTags('role')
@Controller('role')
export class RoleController {
  constructor(private readonly roleService: RoleService) {}

  @Get()
  async getRoles(): Promise<Roles[]> {
    return this.roleService.findAll();
  }

  // @Get(':username')
  // async getProfile(@User('id') userId: number, @Param('username') username: string): Promise<ProfileRO> {
  //     return await this.profileService.findProfile(userId, username);
  // }

  // @Post(':username/follow')
  // async follow(@User('email') email: string, @Param('username') username: string): Promise<ProfileRO> {
  //     return await this.profileService.follow(email, username);
  // }

  // @Delete(':username/follow')
  // async unFollow(@User('id') userId: number, @Param('username') username: string): Promise<ProfileRO> {
  //     return await this.profileService.unFollow(userId, username);
  // }
}
