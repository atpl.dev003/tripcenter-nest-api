import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from 'typeorm';
import { Countries } from '../countries/Countries';

@Entity('CoachOperatorRegistrations', { schema: 'dbo' })
export class CoachOperatorRegistrations {
  @Column('uniqueidentifier', {
    nullable: false,
    primary: true,
    name: 'Id'
  })
  Id: string;

  @Column('datetime', {
    nullable: false,
    name: 'DateAdded'
  })
  DateAdded: Date;

  @Column('nvarchar', {
    nullable: false,
    length: 200,
    name: 'Email'
  })
  Email: string;

  @Column('nvarchar', {
    nullable: false,
    length: 200,
    name: 'FirstName'
  })
  FirstName: string;

  @Column('nvarchar', {
    nullable: false,
    length: 200,
    name: 'LastName'
  })
  LastName: string;

  @Column('nvarchar', {
    nullable: false,
    length: 200,
    name: 'CompanyName'
  })
  CompanyName: string;

  @Column('nvarchar', {
    nullable: false,
    length: 200,
    name: 'AddressLine1'
  })
  AddressLine1: string;

  @Column('nvarchar', {
    nullable: true,
    length: 200,
    name: 'AddressLine2'
  })
  AddressLine2: string | null;

  @Column('nvarchar', {
    nullable: false,
    length: 200,
    name: 'City'
  })
  City: string;

  @ManyToOne(
    () => Countries,
    (Countries: Countries) => Countries.coachOperatorRegistrationss,
    { nullable: false }
  )
  @JoinColumn({ name: 'Country' })
  country: Countries | null;

  @Column('nvarchar', {
    nullable: false,
    length: 200,
    name: 'Postcode'
  })
  Postcode: string;

  @Column('nvarchar', {
    nullable: true,
    length: 200,
    name: 'LandlineNumber'
  })
  LandlineNumber: string | null;

  @Column('nvarchar', {
    nullable: true,
    length: 200,
    name: 'FaxNumber'
  })
  FaxNumber: string | null;

  @Column('nvarchar', {
    nullable: false,
    length: 200,
    name: 'MobileNumber'
  })
  MobileNumber: string;

  @Column('nvarchar', {
    nullable: true,
    length: 200,
    name: 'Website'
  })
  Website: string | null;

  @Column('nvarchar', {
    nullable: false,
    length: 200,
    name: 'CompanyRegistration'
  })
  CompanyRegistration: string;

  @Column('nvarchar', {
    nullable: false,
    length: 200,
    name: 'TaxNumber'
  })
  TaxNumber: string;

  @Column('datetime', {
    nullable: true,
    name: 'DateApproved'
  })
  DateApproved: Date | null;

  @Column('datetime', {
    nullable: true,
    name: 'DateRejected'
  })
  DateRejected: Date | null;

  @Column('nvarchar', {
    nullable: true,
    name: 'ReasonRejected'
  })
  ReasonRejected: string | null;

  @Column('nchar', {
    nullable: true,
    length: 50,
    name: 'CompanyCategory'
  })
  CompanyCategory: string | null;

  @Column('nchar', {
    nullable: true,
    length: 100,
    name: 'State'
  })
  State: string | null;

  @Column('nchar', {
    nullable: true,
    length: 50,
    name: 'PhoneNumber'
  })
  PhoneNumber: string | null;

  @Column('nchar', {
    nullable: true,
    length: 50,
    name: 'EmergencyMobileNumber'
  })
  EmergencyMobileNumber: string | null;

  @Column('nvarchar', {
    nullable: true,
    length: 50,
    name: 'CustomerHelpline'
  })
  CustomerHelpline: string | null;

  @Column('nchar', {
    nullable: true,
    length: 10,
    name: 'Salutation'
  })
  Salutation: string | null;

  @Column('nchar', {
    nullable: true,
    length: 255,
    name: 'JobTitle'
  })
  JobTitle: string | null;

  @Column('nchar', {
    nullable: true,
    length: 10,
    name: 'Currency'
  })
  Currency: string | null;

  @Column('nchar', {
    nullable: true,
    length: 100,
    name: 'Distance'
  })
  Distance: string | null;

  @Column('nchar', {
    nullable: true,
    length: 255,
    name: 'HearAbout'
  })
  HearAbout: string | null;

  @Column('nchar', {
    nullable: true,
    length: 255,
    name: 'AssociationName'
  })
  AssociationName: string | null;

  @Column('nchar', {
    nullable: true,
    length: 255,
    name: 'MemberShip'
  })
  MemberShip: string | null;

  @Column('nchar', {
    nullable: true,
    length: 10,
    name: 'CountryTelCode'
  })
  CountryTelCode: string | null;

  @Column('nchar', {
    nullable: true,
    length: 10,
    name: 'FaxCountryTelCode'
  })
  FaxCountryTelCode: string | null;
}
