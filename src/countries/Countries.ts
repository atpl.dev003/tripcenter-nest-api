import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId
} from 'typeorm';
import { CoachOperatorRegistrations } from '../coachOperatorRegistration/CoachOperatorRegistrations';
// import {Bookings} from "./Bookings";
// import {ContactDetailses} from "./ContactDetailses";
// import {Depots} from "./Depots";
// import {ItineraryPoints} from "./ItineraryPoints";
// import {TourGuideRegistrations} from "./TourGuideRegistrations";
// import {TourOperatorRegistrations} from "./TourOperatorRegistrations";
// import {TransferLocations} from "./TransferLocations";

@Entity('Countries', { schema: 'dbo' })
export class Countries {
  @Column('uniqueidentifier', {
    nullable: false,
    primary: true,
    name: 'Id'
  })
  Id: string;

  @Column('nvarchar', {
    nullable: false,
    length: 200,
    name: 'Name'
  })
  Name: string;

  @Column('nvarchar', {
    nullable: false,
    length: 200,
    name: 'IsoCode'
  })
  IsoCode: string;

  @Column('nvarchar', {
    nullable: true,
    length: 200,
    name: 'RSSFeed'
  })
  RSSFeed: string | null;

  // @OneToMany(()=>Bookings, (Bookings: Bookings)=>Bookings.country)
  // bookingss:Bookings[];

  // @OneToMany(()=>Bookings, (Bookings: Bookings)=>Bookings.passengerNationality)
  // bookingss2:Bookings[];

  @OneToMany(
    () => CoachOperatorRegistrations,
    (CoachOperatorRegistrations: CoachOperatorRegistrations) =>
      CoachOperatorRegistrations.country
  )
  coachOperatorRegistrationss: CoachOperatorRegistrations[];

  // @OneToMany(()=>ContactDetailses, (ContactDetailses: ContactDetailses)=>ContactDetailses.country)
  // contactDetailsess:ContactDetailses[];

  // @OneToMany(()=>Depots, (Depots: Depots)=>Depots.country)
  // depotss:Depots[];

  // @OneToMany(()=>ItineraryPoints, (ItineraryPoints: ItineraryPoints)=>ItineraryPoints.country)
  // itineraryPointss:ItineraryPoints[];

  // @OneToMany(()=>TourGuideRegistrations, (TourGuideRegistrations: TourGuideRegistrations)=>TourGuideRegistrations.country)
  // tourGuideRegistrationss:TourGuideRegistrations[];

  // @OneToMany(()=>TourOperatorRegistrations, (TourOperatorRegistrations: TourOperatorRegistrations)=>TourOperatorRegistrations.country)
  // tourOperatorRegistrationss:TourOperatorRegistrations[];

  // @OneToMany(()=>TransferLocations, (TransferLocations: TransferLocations)=>TransferLocations.country)
  // transferLocationss:TransferLocations[];
}
