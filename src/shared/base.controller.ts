import { SECRET } from '../config';
import * as jwt from 'jsonwebtoken';
import { BaseResponse } from './interfaces/IBaseResponse';

export class BaseController {
  CONST_SUPPLIER_ROLES: string[];
  CONST_CMS_ROLES: string[];
  constructor() {
    this.CONST_SUPPLIER_ROLES = ['55FDAD83-BDB8-4EF8-8B74-B249C7D0F5DB'];
    this.CONST_CMS_ROLES = ['55FDAD83-BDB8-4EF8-8B74-B249C7D0F5DB'];
  }

  protected getUserIdFromToken(authorization) {
    if (!authorization) return null;

    const token = authorization.split(' ')[1];
    const decoded: any = jwt.verify(token, SECRET);
    return decoded.id;
  }

  protected buildResponse(status: number, msg?: string, data?: any) {
    const res = new BaseResponse<any>();
    res.statusCode = status;
    res.data = data ? data : null;
    res.message = msg ? msg : '';
    return res;
  }
}
