import * as crypto from 'crypto';

export function encryptString(str: string) {
  return crypto.createHmac('sha256', str).digest('hex');
}

export function decryptString(str: string) {
  return crypto.createHmac('sha256', str).digest('hex');
}
