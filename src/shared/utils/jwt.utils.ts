import * as jwt from 'jsonwebtoken';
import { SECRET } from '../../config';
import { Users } from '../../users/users.entity';
import { UserRO } from '../../users/users.interface';

const sessionTimeOutDay = 60;

export function generateJWT(payload: Users) {
  let today = new Date();
  let exp = new Date(today);
  exp.setDate(today.getDate() + sessionTimeOutDay);

  let _userRo: UserRO;
  _userRo.Id = payload.Id;
  _userRo.FName = payload.fName;
  _userRo.LName = payload.lName;
  _userRo.Name = `${payload.fName} ${payload.lName}`;
  _userRo.Role = payload.role;
  _userRo.Telephone = payload.TelPhone;
  _userRo.CountryCode = payload.CountryCode;
  _userRo.Currency = payload.Currency;
  _userRo.Department = payload.Department;
  _userRo.Email = payload.Email;
  _userRo.JobTitle = payload.JobTitle;

  const token = jwt.sign(
    {
      ..._userRo,
      exp: exp.getTime() / 1000
    },
    SECRET
  );
  _userRo.Token = token;
  return { _userRo, token };
}

export function decodeJWT(token) {
  return jwt.verify(token, SECRET);
}
