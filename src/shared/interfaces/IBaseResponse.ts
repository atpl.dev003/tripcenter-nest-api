export interface IBaseResponse<T> {
  statusCode: number;
  message: string;
  data: T;
}

export class BaseResponse<T> {
  statusCode: number;
  message: string;
  data: T;
}
